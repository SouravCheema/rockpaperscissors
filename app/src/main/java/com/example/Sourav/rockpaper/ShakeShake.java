
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.seismic.ShakeDetector;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ShakeShake extends AppCompatActivity implements ShakeDetector.Listener, View.OnClickListener {

    public static String ARG_PLAYER_NAME = "";
//    public static String DATA_PLAYER_NAME = "ARG_PLAYER_NAME";

    ShakeDetector detector;
    TextView Choice;
    FirebaseDatabase database;
    DatabaseReference root;
    SharedPreferences pref;
    Button btnResult;

    String[] RPS = {"Rock","Paper","Scissor"};

    String player = "";
    String choice = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shake_shake);

        btnResult = (Button)findViewById(R.id.btnResult);
        btnResult.setOnClickListener(this);

        pref = getSharedPreferences("MyPref",0);

        Choice = (TextView)findViewById(R.id.txtChange);

        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        detector = new ShakeDetector(this);
        detector.start(manager);

        player = getIntent().getStringExtra(ARG_PLAYER_NAME);
        root = FirebaseDatabase.getInstance().getReference();


    }

    @Override
    public void hearShake() {
        Log.d("SHAKE","Phone Shaked");
        Toast.makeText(this,"Phone is shaking",Toast.LENGTH_SHORT).show();

        int rnd = new Random().nextInt(RPS.length);
        String value = RPS[rnd];

        Choice.setText(String.valueOf(value));

//        database = FirebaseDatabase.getInstance();
//        root = database.getReference();

        Log.d("Choice................................",value);

        //SharedPreferences.Editor editor = pref.edit();

////        String id = pref.getString("id",null);
//
//        Log.d("Id......................",id);


        String id = pref.getString("id",null);
        User user = new User(player,value);
        DatabaseReference a = root.child(id).push();
        a.setValue(user);
//        DatabaseReference ref=FirebaseDatabase.getInstance().getReference().child(id).child(player);
//        Map<String, Object> updates = new HashMap<String,Object>();
//        updates.put("Choice", value);
//
//
//       ref.updateChildren(updates);





        detector.stop();

//        Intent newIntent = new Intent(ShakeShake.this,ResultGame.class);
//        startActivity(newIntent);
    }

    @Override
    public void onClick(View view) {


        Intent newIntent = new Intent(ShakeShake.this,ResultGame.class);
        startActivity(newIntent);

    }
}
