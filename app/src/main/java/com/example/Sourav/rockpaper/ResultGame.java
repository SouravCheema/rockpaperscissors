

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ResultGame extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference,dbr;
    List<User> userList;
    String id,firstChoice,secondChoice;

    TextView textFinalName, textFinalScore;

    ListView ResultList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_game);


        sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //id = String.valueOf(sharedPreferences.getInt("id",0));
        firebaseDatabase = FirebaseDatabase.getInstance();
        id = RockApplication.getInstance().getGameID();
        databaseReference = firebaseDatabase.getReference(id);


        textFinalName = (TextView)findViewById(R.id.txtFinalName);
        textFinalScore = (TextView)findViewById(R.id.txtFinalScore);

        ResultList = (ListView)findViewById(R.id.list1);
        userList = new ArrayList<>();
        start();

    }

    public void start(){

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //userList.clear();
                for(DataSnapshot userSnapshot: dataSnapshot.getChildren())
                {
                    User user = userSnapshot.getValue(User.class);
                    userList.add(user);
                    Log.d("DATA", String.valueOf(userList));
                }
                UserList adapter = new UserList(ResultGame.this,userList);
                ResultList.setAdapter(adapter);

                showResult();
                //textFinalName.setText(userList.get(1).getChoice());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }




    private void showResult() {


        Log.d("userList",userList.toString());
        firstChoice = userList.get(0).getChoice().toUpperCase();
        secondChoice = userList.get(1).getChoice().toUpperCase();



        if(firstChoice.equals(secondChoice))
        {

            textFinalName.setText("Draw");


        }
        else if (firstChoice.equals("ROCK") && secondChoice.equals("SCISSOR"))
        {

            textFinalName.setText("WINNER : " + firstChoice);


        }

        else if (firstChoice.equals("ROCK") && secondChoice.equals("PAPER"))
        {

            textFinalName.setText("WINNER : " + secondChoice);


        }

        else if (firstChoice.equals("SCISSOR") && secondChoice.equals("ROCK"))
        {

            textFinalName.setText("WINNER : " + secondChoice);


        }

        else if (firstChoice.equals("SCISSOR") && secondChoice.equals("PAPER"))
        {

            textFinalName.setText("WINNER : " + firstChoice);


        }

        else if (firstChoice.equals("PAPER") &&secondChoice.equals("ROCK"))
        {

            textFinalName.setText("WINNER : " + firstChoice);


        }

        else if (firstChoice.equals("PAPER") && secondChoice.equals("SCISSOR"))
        {

            textFinalName.setText("WINNER : " + userList.get(1).getName());


        }

        // }

    }
}


