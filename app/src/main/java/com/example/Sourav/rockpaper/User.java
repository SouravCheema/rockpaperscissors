

public class User {



    public String name;

    public String choice;
    public int score;



    public User(){

    }

    public User(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public User(String name, String choice) {
        this.name = name;
        this.choice = choice;
    }

    public User(String choice) {

        this.choice = choice;
    }

    public User(String name,  String choice, int score) {
        this.name = name;
        this.choice = choice;
        this.score = score;

    }

    public String getName() {
        return name;
    }


    public int getScore() {
        return score;
    }

    public String getChoice() {
        return choice;
    }

}
