import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class UserList extends ArrayAdapter<User> {

    private Activity context;
    private List<User> userList;

    public UserList(Activity context, List<User> userList)
    {
        super(context,R.layout.activity_list_view,userList);
        this.context = context;
        this.userList = userList;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.activity_list_view,null,true);
        TextView textViewName = (TextView)listViewItem.findViewById(R.id.txt11);
        TextView textViewResult = (TextView)listViewItem.findViewById(R.id.txt22);
        User user = userList.get(position);
        textViewName.setText(user.getName());
        textViewResult.setText(user.getChoice());

        return listViewItem;

    }
}

