

import android.app.Application;

import java.util.Random;
import java.util.UUID;

public class RockApplication extends Application {

    private  static RockApplication sInstance;

    private String gameID = "";

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;

        Random randnumber = new Random();
        gameID = String.valueOf(100000 + randnumber.nextInt(100000));
    }

    public static RockApplication getInstance() {
        return sInstance;
    }

    public String getGameID() {
        if (gameID.isEmpty()){
            Random randnumber = new Random();
            gameID = String.valueOf(100000 + randnumber.nextInt(100000));
        }
        return gameID;
    }
}
